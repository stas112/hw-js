//+Реализовать функцию для создания объекта "пользователь".
//+Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
//+ При вызове функция должна спросить у вызывающего имя и фамилию.
// +Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
// +Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя,
// +соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// +Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin().
// +Вывести в консоль результат выполнения функции.
// +Создать метод getAge() который будет возвращать сколько пользователю лет.
// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
// Возьмите выполненное задание выше (созданная вами функция createNewUser()) и дополните ее следующим функционалом: 
// При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.

/*class CreateNewUser {
    constructor(firstName, lastName,birthday){
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
    }
    getLogin(){
        return `${this.firstName[0]}${this.lastName}`.toLowerCase();
    }
    getAge(){
        return new Date().getFullYear() - this.birthday.split(".")[2];
    }
    getPassword(){
        return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.split(".")[2] );
    }
  

}



const newUser = new CreateNewUser(prompt("введіть ваше імя"), prompt("введіть вашу фамілію"),prompt("введіть дату народження","dd.mm.yyyy" )); 
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());

*/


let filterBy = function(arr,dataType){
      return arr.filter( (item) => {
            return typeof item != typeof dataType
        } )
}

document.write(filterBy([1,3,5,5,6,'ggg',{},[],"gg"],5));
